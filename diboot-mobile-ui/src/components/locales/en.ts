import type { Locale } from './zhCN'

const en: Locale = {
  components: {
    select: {
      keyword: 'Please Input Keyword',
      searchOption: 'Search Option',
      noOption: 'Empty'
    },
    rich: {
      editor: {
        placeholder: 'Please Input Content...'
      }
    }
  },
  di: {
    input: {
      date: 'Select date',
      time: 'Select time',
      next: 'Next',
      uploadFile: 'Upload file',
      fileLarge: 'The file size cannot exceed {0}MB'
    }
  }
}

export default en
